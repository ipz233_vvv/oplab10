#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <Windows.h>

int main() {
	SetConsoleOutputCP(1251);
	printf("������ ���� ��� ���������\n");
	int max_num=0, max_sum=0;
	while (1) {
		int x;
		scanf("%d", &x);
		if (x == 0) {
			break;
		}
		if (x < 0) {
			printf("�������� ������ ���� � ��������� �������\n");
			continue;
		}
		int xc = x;
		int s = 0;
		while (xc != 0) {
			s += xc % 10;
			xc /= 10;
		}
		if (s > max_sum) {
			max_num = x;
			max_sum = s;
		}
	}
	printf("�������� ���� ���� (%d) � ����� %d", max_sum, max_num);
	return 0;
}