#include <stdio.h>

int main() {
	int repeat = 4;
	for (int i = 2; i < (repeat+1)*2; i++) {
		for (int j = 0; j < i/2; j++) {
			printf("%d",i/2 + 2*(i%2));
		}
		printf("\n", i);
	}	
	return 0;
}