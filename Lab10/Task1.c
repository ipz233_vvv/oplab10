#include <stdio.h>

int main() {
	float s = 0;
	for (int i = 1; i <= 50; i++) {
		for (float j = 1; j <= 20; j++) {
			s += 1.0 / (i + j);
		}
	}
	printf("%f", s);
	return 0;
}